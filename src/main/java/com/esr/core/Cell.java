package com.esr.core;

public class Cell {

    private final static String DEAD = "O";
    private final static String ALIVE = "\u25A0";

    private boolean currentState;
    private boolean newState;

    public Cell(boolean state) {
        currentState = state;
        newState = state;
    }

    public void setNewState(boolean state) {
        newState = state;
    }

    public void updateState() {
        currentState = newState;
    }

    public boolean isAlive() {
        return currentState;
    }

    @Override
    public String toString() {
        if (isAlive())
            return ALIVE;
        else
            return DEAD;
    }
}