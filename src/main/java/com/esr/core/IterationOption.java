package com.esr.core;

import com.esr.GameOfLife;
import com.esr.abstracts.ConfigOption;

import static com.esr.util.GestorIO.io;

public class IterationOption extends ConfigOption {

    public IterationOption(GameOfLife gol) {
        super("Number of iterations.", gol);
    }

    @Override
    public void show(int position) {
        super.show(position);
        if (gol.getIterations() > 0) {
            System.out.print(" [" + gol.getIterations() + "]");
        }
    }

    @Override
    public void execute() {
        int iterations;
        boolean error;
        do {
            io.out("Number of iterations? [1-n]: ");
            iterations = io.inInt();
            error = iterations <= 0;
            if (error) {
                io.out("Error!!! The iterations must be great than zero.");
            }
        } while (error);

        gol.setIterations(iterations);
    }

}
