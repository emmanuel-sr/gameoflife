package com.esr.core;

import com.esr.GameOfLife;
import com.esr.abstracts.ConfigOption;

import static com.esr.util.GestorIO.io;

public class DimensionOption extends ConfigOption {

    public DimensionOption(GameOfLife gol) {
        super("Board dimension.", gol);
    }

    @Override
    public void show(int position) {
        super.show(position);
        if (gol.getRows() >= 0 && gol.getCols() >= 0)
            io.out(" [" + gol.getRows() + ", " +
                    gol.getCols() + "]");
    }

    @Override
    public void execute() {
        int rows;
        int cols;
        boolean error;
        do {
            io.out("\nrows?: ");
            rows = io.inInt();

            io.out("\ncols?: ");
            cols = io.inInt();

            error = rows <= 0 || cols <= 0;
            if (error) {
                io.out("\nError!!! The dimensions must be greater than or equal to 1x1.");
            }
        } while (error);

        gol.setRows(rows);
        gol.setCols(cols);

    }
}
