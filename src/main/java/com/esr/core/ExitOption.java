package com.esr.core;

import com.esr.abstracts.Option;

public class ExitOption extends Option {

    private boolean state;

    public ExitOption() {
        super("Exit.");
        state = false;
    }

    @Override
    public void execute() {
        setState(true);
    }

    public boolean executed() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
