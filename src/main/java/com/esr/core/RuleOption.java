package com.esr.core;

import com.esr.GameOfLife;
import com.esr.abstracts.ConfigOption;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static com.esr.util.GestorIO.io;

public class RuleOption extends ConfigOption {

    public RuleOption(GameOfLife gol) {
        super("Rules.", gol);
    }

    @Override
    public void show(int position) {
        super.show(position);
        if (gol.getRules() != null && !gol.getRules().isEmpty())
            io.out(" [x]");
    }

    @Override
    public void execute() {
        List<Rule> rules = null;
        boolean error;
        do {
            io.out("\nEnter the file path:");
            String path = io.inString();
            Gson gson = new Gson();
            try {
                rules = gson.fromJson(new BufferedReader(
                        new FileReader(path)), new TypeToken<ArrayList<Rule>>() {
                }.getType());
                error = false;
            } catch (FileNotFoundException | JsonSyntaxException ex) {
                error = true;
                io.out("\nError!!! JSON syntax exception");
            }
        } while (error);
        gol.setRules(rules);
    }
}
