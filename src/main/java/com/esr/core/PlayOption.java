package com.esr.core;

import com.esr.GameOfLife;
import com.esr.abstracts.Option;

import static com.esr.util.GestorIO.io;

public class PlayOption extends Option {

    private GameOfLife gol;

    public PlayOption(GameOfLife gol) {
        super("Play.");
        this.gol = gol;
    }

    @Override
    public void show(int position) {
        super.show(position);
        if (gol.isReady())
            io.out(" (Let´s go!)");
    }

    @Override
    public void execute() {
        if (gol.isReady()) {
            gol.init();
            gol.play();
        }else{
            io.out("\nError!!! complete the required configuration.");
        }
    }
}
