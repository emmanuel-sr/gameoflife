package com.esr.core;

import com.esr.abstracts.Option;

import java.util.ArrayList;
import java.util.List;

import static com.esr.util.GestorIO.io;

public class Menu {

    private List<Option> options;
    private ExitOption exit;

    public Menu() {
        options = new ArrayList<>();
        exit = new ExitOption();
        options.add(exit);
    }

    public void add(Option option) {
        options.add(option);
    }

    public void show() {
        for (int i = 0; i < options.size(); i++) {
            options.get(i).show(i + 1);
        }
    }

    public boolean finished() {
        return exit.executed();
    }

    public Option getOption() {
        int option;
        boolean error;
        do {
            System.out.print("\nOption? [1-" + options.size() + "]: ");
            option = io.inInt();
            error = option < 1 || option > options.size();
            if (error) {
                io.out("\nError!!! The option must be between 1 and " + options.size());
            }
        } while (error);
        return options.get(option - 1);
    }

}
