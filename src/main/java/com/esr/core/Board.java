package com.esr.core;

import com.esr.util.Coordinate;

import java.util.List;

import static com.esr.util.GestorIO.io;

public class Board {

    private Cell[][] grid;

    public Board() {
    }

    public void init(int rows, int cols, List<Coordinate> coordinates) {
        grid = new Cell[rows][cols];

        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                grid[i][j] = new Cell(false);
            }
        }

        for (Coordinate c : coordinates) {
            if (isValid(c)) {
                grid[c.getX()][c.getY()].setNewState(true);
                grid[c.getX()][c.getY()].updateState();
            }
        }
    }

    private boolean isValid(Coordinate c) {
        return c.getX() >= 0 && c.getX() < getRows()
                && c.getY() >= 0 && c.getY() < getCols();
    }

    public void show() {
        for (Cell[] row : grid) {
            io.out("\n");
            for (Cell cell : row) {
                io.out(cell + " ");
            }
        }
        io.out("\n");
    }

    public void update() {
        for (Cell[] row : grid) {
            for (Cell cell : row)
                cell.updateState();
        }
    }

    public int getRows() {
        return grid.length;
    }

    public int getCols() {
        return grid[0].length;
    }

    public Cell[][] getGrid() {
        return grid;
    }

}
