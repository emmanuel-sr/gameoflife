package com.esr.core;

import com.esr.GameOfLife;
import com.esr.abstracts.ConfigOption;
import com.esr.util.Coordinate;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.Arrays;
import java.util.List;

import static com.esr.util.GestorIO.io;

public class CoordinateOption extends ConfigOption {

    public CoordinateOption(GameOfLife gol) {
        super("Coordinates.", gol);
    }

    @Override
    public void show(int position) {
        super.show(position);
        if (gol.getCoordinates() != null && !gol.getCoordinates().isEmpty())
            io.out(" [x]");
    }

    @Override
    public void execute() {
        List<Coordinate> coordinates = null;
        boolean error;
        do {
            io.out("\nEnter an array of coordinates in json format: ");
            String json = io.inString();
            Gson gson = new Gson();
            try {
                coordinates = Arrays.asList(gson.fromJson(json, Coordinate[].class));
                error = false;
            } catch (JsonSyntaxException ex) {
                error = true;
                io.out("\nError!!! JSON syntax exception");
            }
        } while (error);
        gol.setCoordinates(coordinates);
    }
}
