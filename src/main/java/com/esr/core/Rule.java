package com.esr.core;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Rule {

    private Type type;
    private List<Integer> neighbors;

    public void apply(Cell cell, int neighbors) {
        if ((cell.isAlive() && type.equals(Type.SURVIVE)) ||
                (!cell.isAlive() && type.equals(Type.BORN))) {
            cell.setNewState(this.neighbors.contains(neighbors));
        }
    }

    enum Type {
        SURVIVE,
        BORN
    }

}
