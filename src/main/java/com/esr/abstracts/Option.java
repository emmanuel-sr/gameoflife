package com.esr.abstracts;

import static com.esr.util.GestorIO.io;

public abstract class Option {

    protected final String title;

    protected Option(String title) {
        this.title = title;
    }

    public void show(int position) {
        io.out("\n" + position + ". " + title);
    }

    public abstract void execute();

}