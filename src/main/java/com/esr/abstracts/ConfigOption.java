package com.esr.abstracts;

import com.esr.GameOfLife;

public abstract class ConfigOption extends Option {

    protected GameOfLife gol;

    public ConfigOption(String title, GameOfLife gol) {
        super(title);
        this.gol = gol;
    }
}