package com.esr.util;

import java.util.Scanner;

public class GestorIO {

    private static Scanner s = new Scanner(System.in);

    public static GestorIO io = new GestorIO();

    private GestorIO(){
    }

    public String inString ()
    {
        String input = null;
        try
        {
            input = s.nextLine();
        }
        catch (Exception e)
        {
            this.error();
        }
        return input;
    }

    public int inInt ()
    {
        int input = 0;
        try
        {
            input = Integer.parseInt(this.inString());
        }
        catch (Exception e)
        {
            this.error();
        }
        return input;
    }

    public void out (String str)
    {
        System.out.print(str);
    }

    private void error(){
        out("\nError!!! Input error.");
    }

}
