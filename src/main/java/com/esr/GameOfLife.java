package com.esr;

import com.esr.core.*;
import com.esr.util.Coordinate;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.esr.util.GestorIO.io;

@Data
public class GameOfLife {

    private final static int[][] dirs = {{0, -1}, {0, 1}, {1, 0}, {-1, 0}, {1, 1}, {-1, 1}, {1, -1}, {-1, -1}};

    private Menu menu;
    private Board board;
    private int iterations;
    private int rows;
    private int cols;
    private List<Coordinate> coordinates;
    private List<Rule> rules;

    public GameOfLife() {
        menu = new Menu();
        board = new Board();
        coordinates = new ArrayList<>();

        menu.add(new PlayOption(this));
        menu.add(new IterationOption(this));
        menu.add(new DimensionOption(this));
        menu.add(new CoordinateOption(this));
        menu.add(new RuleOption(this));
    }

    public void start() {
        do {
            menu.show();
            menu.getOption().execute();
        } while (!menu.finished());
    }

    public void init() {
        board.init(rows, cols, coordinates);
    }

    public void play() {
        io.out("\nInitial state:");
        board.show();

        for (int i = 1; i <= iterations; i++) {
            io.out("\n\nPress \"ENTER\" to continue...");
            new Scanner(System.in).nextLine();
            io.out("Iteration: " + i);
            compute();
            board.update();
            board.show();
        }
    }

    public void compute() {
        for (Rule rule : rules) {
            for (int i = 0; i < board.getRows(); i++) {
                for (int j = 0; j < board.getCols(); j++) {
                    int neighbors = countNeighbors(board.getGrid(), i, j);
                    rule.apply(board.getGrid()[i][j], neighbors);
                }
            }
        }
    }

    private static int countNeighbors(Cell[][] grid, int row, int col) {
        int count = 0;
        for (int[] dir : dirs) {
            int newRow = row + dir[0];
            int newCol = col + dir[1];

            if (newRow >= 0 && newRow < grid.length && newCol >= 0 && newCol < grid[0].length
                    && grid[newRow][newCol].isAlive()) {
                count++;
            }
        }
        return count;
    }

    public boolean isReady() {
        return iterations > 0 && cols > 0 && rows > 0 && coordinates != null && rules != null;
    }

    public static void main(String[] args) {
        new GameOfLife().start();
    }

}
